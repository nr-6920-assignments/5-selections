import os
import re
import shutil
import subprocess
import sys
import unittest
import nbformat
from nbconvert import HTMLExporter
from nbconvert.preprocessors import CellExecutionError, ExecutePreprocessor

STREAM = sys.stdout
STREAM_STRONG = sys.stderr

def nb2html(in_fn, out_fn):
    html_exporter = HTMLExporter()
    html_exporter.template_name = 'classic'
    body, resources = html_exporter.from_filename(in_fn)
    with open(out_fn, 'w') as fp:
        fp.write(body)


class NotebookError(Exception):
    pass


class ProblemTestResult(unittest.TextTestResult):
    def __init__(self, stream, descriptions, verbosity):
        super().__init__(stream, True, 2)
    def getDescription(self, test):
        doc_first_line = test.shortDescription()
        if self.descriptions and doc_first_line:
            return doc_first_line
        else:
            return str(test)


class BaseTester(unittest.TestCase):
    longMessage = False
    problem = 0
    suffix = ''
    result_filename = ''

    def __init__(self, methodName, use_test_folder=True):
        super().__init__(methodName)
        self.use_test_folder = use_test_folder

    def get_args(self):
        if self.args:
            return '\n' + '\n'.join([f'  {k} = {v}' for k, v in self.args.items()]) + '\n'
        else:
            return ''

    def setUp(self):
        # self.repo_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        self.repo_folder = os.path.normpath(os.path.join(os.path.realpath(__file__), '..', '..', '..'))
        self.test_folder = os.path.join(self.repo_folder, 'tests')
        if self.use_test_folder:
            self.data_folder = os.path.join(self.test_folder, f'data{self.problem}{self.suffix}')
        else:
            self.data_folder = os.path.join(self.repo_folder, 'data')
        self.in_notebook = os.path.join(self.repo_folder, f'problem{self.problem}.ipynb')
        self.out_notebook = os.path.join(
            self.test_folder, f'problem{self.problem}{self.suffix}.ipynb')
        self.notebook_exists = os.path.exists(self.in_notebook)
        self.result_path = os.path.join(self.data_folder, self.result_filename)

    def test_notebook(self, args=None):
        """Run notebook"""
        STREAM.write(self.get_args())
        if not self.notebook_exists:
            self.skipTest(self.in_notebook + ' not found!')
        self.copy_data()
        result = self.execute_notebook()
        if result:
            raise NotebookError(result)

    def copy_data(self):
        in_folder = os.path.join(self.test_folder, 'data')
        if os.path.exists(self.data_folder):
            shutil.rmtree(self.data_folder)
        p = ['xcopy', in_folder, self.data_folder, '/e', '/s', '/i', '/q', '/y']
        subprocess.run(p, check=True, capture_output=True)

    def execute_notebook(self, kernel='arcgispro-py3', html=False):
        try:
            self._execute_notebook(self.in_notebook, self.out_notebook, self.args, kernel=kernel)
            return 0
        except CellExecutionError as e:
            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
            return ansi_escape.sub('', e.args[0])
        finally:
            if html:
                nb2html(out_fn, out_fn.replace('.ipynb', '.html'))

    def notebook_ran(self):
        if self.use_test_folder and not self.notebook_exists:
            self.skipTest('Notebook not found')

    def result_exists(self):
        if not os.path.exists(self.result_path):
            self.skipTest(f'Output file {self.result_path} not found')

    def _execute_notebook(self, in_fn, out_fn, args, kernel):
        ep = ExecutePreprocessor(kernel_name=kernel)
        src = (
            ['# Inserted parameters']
            + [f'{k} = {self._stringify(v)}' for k, v in args.items()]
        )
        nb = nbformat.read(in_fn, as_version=4)
        for i, cell in enumerate(nb.cells):
            if cell.cell_type == 'code':
                break
        nb.cells.insert(i+1, nbformat.v4.new_code_cell('\n'.join(src)))
        try:
            ep.preprocess(nb, {'metadata': {'path': os.path.dirname(in_fn)}})
        finally:
            nbformat.write(nb, out_fn)

    def _stringify(self, x):
        try:
            return f'"{x.encode("unicode-escape").decode()}"'
        except AttributeError:
            return x


class Runner(object):

    @classmethod
    def print(cls, msg, strong=True):
        if strong:
            STREAM_STRONG.write(msg + '\n')
        else:
            STREAM.write(msg + '\n')

    @classmethod
    def run_problem(cls, n, test_classes):
        issues = 0
        for i, test_class in enumerate(test_classes):
            s = cls._make_suite(test_class, True)
            runner = unittest.TextTestRunner(stream=STREAM, resultclass=ProblemTestResult)
            STREAM.write(ProblemTestResult.separator1 + '\n')
            STREAM.write(f'TESTING PROBLEM {n} (test case {i+1} of {len(test_classes)})\n')
            result = runner.run(s)
            issues += len(result.errors) + len(result.failures) + len(result.skipped)

            # No point in running another test case if the notebook won't run
            errors = [x[0]._testMethodName for x in result.errors]
            skipped = [x[0]._testMethodName for x in result.skipped]
            if 'test_notebook' in errors + skipped:
                break
        STREAM_STRONG.write(f'PROBLEM {n} {"FAILED" if issues else "PASSED"}\n')
        # runner.stream.writeln(f'PROBLEM {n} {"FAILED" if issues else "PASSED"}')
        return issues == 0

    @classmethod
    def test_output(cls, n, test_class):
        issues = 0
        s = cls._make_suite(test_class, False)
        runner = unittest.TextTestRunner(stream=STREAM, resultclass=ProblemTestResult)
        STREAM.write(ProblemTestResult.separator1 + '\n')
        STREAM.write(f'TESTING PROBLEM {n} RESULTS\n')
        result = runner.run(s)
        issues += len(result.errors) + len(result.failures) + len(result.skipped)
        STREAM_STRONG.write(f'PROBLEM {n} {"FAILED" if issues else "PASSED"}\n')
        # runner.stream.writeln(f'PROBLEM {n} {"FAILED" if issues else "PASSED"}')
        return issues == 0

    @classmethod
    def _make_suite(cls, test_class, run_notebook):
        suite = unittest.TestSuite()
        if run_notebook:
            suite.addTest(test_class('test_notebook', run_notebook))
        for name in unittest.defaultTestLoader.getTestCaseNames(test_class):
            if name != 'test_notebook':
                suite.addTest(test_class(name, run_notebook))
        return suite
