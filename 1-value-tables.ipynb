{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 5-selections\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\5-selections\\data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Last week I left some extra code out of this setup cell because I thought ArcGIS didn't need it anymore. But for whatever reason it didn't work for at least two people, even though it worked for me. So I'm putting the code back in, and you can read the comments if you're interested in what each bit is for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import arcpy and set the workspace.\n",
    "import arcpy\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True\n",
    "\n",
    "# Tell ArcGIS to look in your assignment folder for modules so that\n",
    "# it can find classtools. Jupyter will already look there and \n",
    "# doesn't actually need this.\n",
    "import os\n",
    "import sys\n",
    "sys.path.append(os.path.dirname(arcpy.env.workspace))\n",
    "\n",
    "# Import classtools.\n",
    "import classtools\n",
    "\n",
    "# Set up plotting in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tool results\n",
    "\n",
    "Last week I said that I was going to store the results of geoprocessing tools in a `results` variable, because I think the new printed output in this version of ArcGIS is distracting. I still think that, but instead of storing the results in a variable, I'm going to print them instead. That will usually print out the filename of the file that was created with the tool.\n",
    "\n",
    "In your own work, you can just run the tool and see the output, capture the output in a variable, or print it. Here are the three options:\n",
    "\n",
    "```python\n",
    "arcpy.SomeGeoprocessingTool()           # Just run the tool\n",
    "result = arcpy.SomeGeoprocessingTool()  # Capture the output in a variable\n",
    "print(arcpy.SomeGeoprocessingTool())    # Print the output\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Value tables\n",
    "\n",
    "[ValueTable](https://pro.arcgis.com/en/pro-app/arcpy/classes/valuetable.htm) objects are used for tool parameters that can have **multiple input values**. For example, the [Union](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/union.htm) tool requires a value table for the `in_features` parameter, because there will be more than one input feature class to the tool. \n",
    "\n",
    "![value table parameter](images/union_vt.png)\n",
    "\n",
    "Value tables are just containers for data. A common error I see in the homework is that when faced with a problem that requires a value table, students try to use the exact same code that the notebook uses, including the same geoprocessing tool. Just because you might have data for a certain class on a flash drive doesn't mean that you can't use that same flash drive for other things that are completely unrelated to that class. The same goes for a value table. You put data in it, but it doesn't have to be anything like the data that you put in it yesterday. Just because you'll see it used with the Union tool doesn't mean that you have to use the Union tool in order to use a value table for something else."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Single column value tables\n",
    "\n",
    "Value tables can have one or many columns, depending on what tool is using them and its requirements. The Union tool requires a single-column value table, and that's what we'll start off with. Then we'll move to an example that uses a multiple-column value table.\n",
    "\n",
    "There are also several ways to represent a value table, and it doesn't matter which one you use. While I personally prefer the *list* method, I'll show you the other two so that you'll know what you're looking at if you see them used somewhere.\n",
    "\n",
    "If you look at the graphic above, you'll see that the `in_features` parameters says this: `[[in_features, {Rank}],...]`. The outer square brackets mean that it needs to be a collection of inputs (it doesn't make any sense to only union one thing, after all!). The inner set of square brackets shows you what each of these inputs needs to look like: `[in_features, {Rank}]`. That means that you can provide a feature set (`in_features`) and an optional `Rank` for each one (the curly braces tell you that `Rank` is optional). The comma and dots tell you that you can include as many inputs as you need.\n",
    "\n",
    "If you were using `Rank`, you'd need a two-column value table because you'd be providing two pieces of information for each input. We're going to ignore `Rank` and only provide the filenames, so we only need one column.\n",
    "\n",
    "In the examples below, we're going to use the Union tool with `cities.shp` and `ZipCodes.shp`, so this is what the value table would look like in table form, with one column and two rows:\n",
    "\n",
    "in_features  |\n",
    "------------ |\n",
    "cities.shp   | \n",
    "ZipCodes.shp | "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### List representation\n",
    "\n",
    "Most of the time you can treat the required value table parameter like a list, and that's what you'll do in this first example to union the cities and zip code shapefiles together. First you need a value table for the `in_features` parameter. As discussed above, this only needs to have one column. A single columns value table can be represented as a simple list, so this is all you'd need to create one matching the table shown above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs_list = ['cities.shp', 'ZipCodes.shp']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The syntax for the [Union](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/union.htm) tool is shown in the screenshot above. Let's use the `inputs_list` you just created with the tool:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use inputs_list to specify the inputs for Union.\n",
    "print(arcpy.analysis.Union(\n",
    "    in_features=inputs_list, \n",
    "    out_feature_class='city_zip.shp',\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could've shortened this code by not using the parameter names and/or sticking the list directly in the call to the tool without creating a temporary variable. The following example does both of these things. Notice that the two input shapefiles still have to be surrounded by square brackets to make a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use a list directly to specify the inputs for Union.\n",
    "print(arcpy.analysis.Union(['cities.shp', 'ZipCodes.shp'], 'city_zip.shp'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that you enabled `overwriteOutput` back at the beginning of the notebook. If you hadn't done that, then this would've failed because you were trying to create a file with the same name as the previous example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### String representation\n",
    "\n",
    "You can also use a string as a value table if you separate the individual values by semicolons, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs_string = 'cities.shp;ZipCodes.shp'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the Union example again, but this time using `inputs_string` as the value table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use inputs_string to specify the inputs for Union.\n",
    "print(arcpy.analysis.Union(\n",
    "    in_features=inputs_string, \n",
    "    out_feature_class='city_zip.shp',\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I prefer using a list, though, because I think it's easier to see what's going on, especially for complicated value tables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Value table objects\n",
    "\n",
    "You can also create an actual [ValueTable](https://pro.arcgis.com/en/pro-app/arcpy/classes/valuetable.htm) object instead of impersonating one with a list or string. Strangely enough, using an actual value table is more work than using one of its substitutes.\n",
    "\n",
    "The first thing to do is create an empty value table with as many columns as you need. In this case you only need one column for storing the shapefile name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a value table object with one column.\n",
    "inputs_vt = arcpy.ValueTable(columns=1)\n",
    "inputs_vt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That will have printed out something like `<ValueTable object at 0x121ef8cc278[0x121efcdc8d0]>`. That doesn't mean much to you, but at least you know that `inputs_vt` is a ValueTable because it tells you so.\n",
    "\n",
    "Now you need to add two rows to the value table, one for each shapefile you want to include in the union. There's only one item per row, so it's easy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add two rows (one for each shapefile) to the value table.\n",
    "inputs_vt.addRow('cities.shp')\n",
    "inputs_vt.addRow('ZipCodes.shp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that your value table is set up, you can use it as the first parameter to [Union](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/union.htm):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use inputs_vt to specify the inputs for Union.\n",
    "print(arcpy.analysis.Union(\n",
    "    in_features=inputs_vt, \n",
    "    out_feature_class='city_zip.shp',\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that string you used earlier? You can get it from a value table object with the `exportToString()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the value table's string representation.\n",
    "inputs_vt.exportToString()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiple column value tables\n",
    "\n",
    "You used a single-column value table for the Union example, but now you're going to use one with two columns in order to transpose some fields in a table. But before you do that, let's take a look at the attribute table first. There's a function in the `classtools` module that imports an attribute table into a [Pandas](http://pandas.pydata.org/) dataframe so that you can look at it here instead of in ArcGIS. So let's take a *really* quick look at some basic Pandas functionality."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Quick Pandas intro\n",
    "\n",
    "Pandas is a popular Python module that's designed for data analysis. It doesn't come with Python automatically, but it does come with ArcGIS. So let's  use the `classtools.table2pd()` function to load the cities attribute table into a Pandas dataframe (similar to an R dataframe, if you're familiar with that), and then use the dataframe's `head()` function to look at the first five rows. *(Obviously you could just open the attribute table if you're reading this in ArcGIS, but doing it this way will work for everyone.)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the classtools module to load an attribute table into a pandas dataframe.\n",
    "atts = classtools.table2pd('cities.shp')\n",
    "\n",
    "# Display the first five rows of the atts dataframe. You can change the number\n",
    "# of rows by passing a number to head(), like head(10).\n",
    "atts.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's easy to limit the output to certain columns if you want to make it a bit easier to read. For example, if you want to see just the NAME column, you can include it in brackets, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display just one column of the atts dataframe.\n",
    "atts['NAME'].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Single columns don't print with the fancy tables, but it's still the same data.\n",
    "\n",
    "If you want multiple columns, separate the names with commas and surround them with two sets of brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display three columns of the atts dataframe. Notice the double brackets.\n",
    "atts[['NAME', 'POPLASTCEN', 'POPLASTEST']].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use the `sort_values()` method to sort by a specific column. Let's print the first five rows of the data after it's been sorted by city name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Grab three columns, sort on the NAME column, and then print out the first five rows.\n",
    "atts[['NAME', 'POPLASTCEN', 'POPLASTEST']].sort_values('NAME').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transpose\n",
    "\n",
    "Now that you've had the briefest Pandas introduction in history, let's get back to the transpose problem. You want to pull out the same three columns that you just printed, but transpose them so you end up with a table that looks more like this:\n",
    "\n",
    "```\n",
    "    NAME      TYPE POPULATION\n",
    "  Alpine    Census       7146\n",
    "  Alpine  Estimate       9884\n",
    "    Alta  Estimate        359\n",
    "    Alta    Census        370\n",
    "Altamont    Census        178    \n",
    "Altamont  Estimate        206\n",
    "```\n",
    "\n",
    "If you compare the table from the last Pandas example with this one, you'll see that the values from the 'POPLASTCEN' and 'POPLASTEST' columns are turned into rows, so that there are two rows for each city. The 'TYPE' column in this new table tells you which column the population number originally came from.\n",
    "\n",
    "To do this, you'll use the [Transpose Fields](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/transpose-fields.htm) geoprocessing tool.\n",
    "\n",
    "```\n",
    "arcpy.management.TransposeFields(in_table, in_field, out_table, \n",
    "    in_transposed_field_name, in_value_field_name, {attribute_fields})\n",
    "```\n",
    "\n",
    "The `in_field` parameter is supposed to be a value table. Here's a screenshot of the documentation, with some color-coded highlighting:\n",
    "\n",
    "![transpose value table](images/transpose_vt.png)\n",
    "\n",
    "Like the Union tool, there is one required column for the value table (`field`) and one optional one (`value`).\n",
    "\n",
    "```\n",
    "[[field, {value}],...]\n",
    "```\n",
    "\n",
    "From the description, you can see that `field` is the name of a column in the original attribute table. This column will be turned into rows. If you want the row to use a different identifier than the column name, you use the `value` item for that.\n",
    "\n",
    "Okay, so you want to transpose two columns and give them new names. Here's a tabular representation of the value table you want to construct. It has two columns because the tool needs two pieces of information: field and value. It has two rows because you want to transpose two columns in the original attribute table. So it's saying that you want to turn two columns (POPLASTCEN and POPLASTEST) in the existing attribute table into rows, and each row will provide the source of its data using the names Census and Estimate instead of POPLASTCEN and POPLASTEST.\n",
    "\n",
    "field (column) | value (new name)\n",
    "-------------- | ----------------\n",
    "POPLASTCEN     | Census\n",
    "POPLASTEST     | Estimate\n",
    "\n",
    "\n",
    "The following examples will show you how to do this using lists, strings, and actual value tables.\n",
    "\n",
    "But before we move on to the examples, here are the values you'll use for each parameter. Look at the original data and the example of the desired output in order to understand what each one is.\n",
    "\n",
    "**Original (cities.shp)**\n",
    "\n",
    "```\n",
    "    NAME  POPLASTCEN  POPLASTEST\n",
    "  Alpine        7146        9884\n",
    "    Alta         370         359\n",
    "Altamont         178         206\n",
    "   Alton         134         153\n",
    "  Amalga         427         509\n",
    "```\n",
    "\n",
    "**Transposed (population.dbf)**\n",
    "\n",
    "```\n",
    "    NAME      TYPE POPULATION\n",
    "  Alpine    Census       7146\n",
    "  Alpine  Estimate       9884\n",
    "    Alta  Estimate        359\n",
    "    Alta    Census        370\n",
    "Altamont    Census        178\n",
    "Altamont  Estimate        206\n",
    "```\n",
    "\n",
    "\n",
    "- `in_table`: The original table to transpose. This is cities.shp.\n",
    "- `in_field`: The value table described above.\n",
    "- `out_table`: The table to create. This will be population.dbf.\n",
    "- `in_transposed_field_name`: The name of a new field that will store the identifiers (the `values` from the value table) for each data point. This is \"TYPE\".\n",
    "- `in_value_field_name`: The name of a new field that will store the value from the transposed columns. This is 'POPULATION'.\n",
    "- `attribute_fields`: A list of other fields to include in the output. You'll also include the 'NAME' column."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### List representation\n",
    "\n",
    "The required value table for the `in_field` parameter to `TransposeFields` has two columns, one for the original column name, and one for the substituted value. You can mimic this with a list of lists. Here are your two lists that specify the original and new names:\n",
    "\n",
    "```py\n",
    "['POPLASTCEN', 'Census']\n",
    "['POPLASTEST', 'Estimate']\n",
    "```\n",
    "\n",
    "You can combine those two lists into a list of lists by putting a comma between each smaller list and surrounding them with another set of brackets, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a list of lists to use as a value table.\n",
    "fields_list = [['POPLASTCEN', 'Census'], ['POPLASTEST', 'Estimate']]\n",
    "\n",
    "# Or here's the same thing but broken into lines so it's easier\n",
    "# to read (especially if there are a ton of things in the list).\n",
    "fields_list = [\n",
    "    ['POPLASTCEN', 'Census'], \n",
    "    ['POPLASTEST', 'Estimate'],\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use your `fields_list` variable to run the transpose:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use a list of lists to transpose the table.\n",
    "print(arcpy.management.TransposeFields(\n",
    "    in_table='cities.shp', \n",
    "    in_field=fields_list, \n",
    "    out_table='population.dbf', \n",
    "    in_transposed_field_name='TYPE', \n",
    "    in_value_field_name='POPULATION', \n",
    "    attribute_fields='NAME'\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now load the resulting DBF into a Pandas dataframe and take a look. *If you're reading this in ArcGIS, population.dbf was probably added to the map under the Standalone Tables section.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the new table into a pandas dataframe and look at the first few rows.\n",
    "data = classtools.table2pd('population.dbf')\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The output file is sorted so that all of the Census rows come before the Estimate rows, so let's sort by NAME to make sure the output looks as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sort the new table by the NAME column.\n",
    "data.sort_values('NAME').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And it does!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### String representation\n",
    "\n",
    "Once again, you can also use a single string if you want. In this case the rows of the value table are still separated by semicolons, and the columns in a single row are separated by a space, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fields_string = 'POPLASTCEN Census;POPLASTEST Estimate'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the string to run the tool:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use a string to transpose the table.\n",
    "print(arcpy.management.TransposeFields(\n",
    "    in_table='cities.shp', \n",
    "    in_field=fields_string, \n",
    "    out_table='population.dbf', \n",
    "    in_transposed_field_name='TYPE', \n",
    "    in_value_field_name='POPULATION', \n",
    "    attribute_fields='NAME'\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One thing to be aware of if you use a string is that you need to include extra quotes if there is a space in your new name. For example, what if you wanted to rename POPLASTCEN to 'Last census' instead of 'Census'? If you made your string like this:\n",
    "\n",
    "```\n",
    "'POPLASTCEN Last Census;POPLASTEST Estimate'\n",
    "```\n",
    "\n",
    "ArcPy wouldn't know how to split that first entry (`POPLASTCEN Last Census`), because there are two spaces instead of one. We can tell just by looking at it, but a computer can't. So you'd help it out by putting another set of quotes around \"Last Census\" to specify that those two words go together, like this (just make sure you use a different type of quotes than what you used around the entire thing!):\n",
    "\n",
    "```\n",
    "'POPLASTCEN \"Last Census\";POPLASTEST Estimate'\n",
    "```\n",
    "\n",
    "Here's an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Transpose using \"Last Census\".\n",
    "print(arcpy.management.TransposeFields(\n",
    "    in_table='cities.shp', \n",
    "    in_field='POPLASTCEN \"Last Census\";POPLASTEST Estimate', \n",
    "    out_table='population.dbf', \n",
    "    in_transposed_field_name='TYPE', \n",
    "    in_value_field_name='POPULATION', \n",
    "    attribute_fields='NAME'\n",
    "))\n",
    "\n",
    "# Look at the attribute table to make sure it used the correct name.\n",
    "classtools.table2pd('population.dbf').sort_values('NAME').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Value table objects\n",
    "\n",
    "To run this same example using an actual value table, you'd create a `ValueTable` object with two columns and then add a row for each column in the shapefile's attribute table that you want to transpose. To add a row, create a string with the original and new name separated by a space, just like you did when using a string representation. Then add that string as the value table row.\n",
    "\n",
    "Because this method also uses a space to separate the old and new names, you also need to use an extra set of quotes here if you want to include spaces in the new name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create value table with two columns, one for the \n",
    "# original column name and one for the new name.\n",
    "fields_vt = arcpy.ValueTable(columns=2)\n",
    "\n",
    "# Add two rows to the value table. Each row has the original\n",
    "# column name and then the new name, separated by a space.\n",
    "fields_vt.addRow('POPLASTCEN \"Last census\"')\n",
    "fields_vt.addRow('POPLASTEST Estimate')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use your `fields_vt` variable as the `in_field` parameter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the value table instead of a list of lists to transpose the table.\n",
    "print(arcpy.management.TransposeFields(\n",
    "    in_table='cities.shp', \n",
    "    in_field=fields_vt, \n",
    "    out_table='population.dbf', \n",
    "    in_transposed_field_name='TYPE', \n",
    "    in_value_field_name='POPULATION', \n",
    "    attribute_fields='NAME'\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "The Summary Statistics tool uses a value table to specify which statistics you want to calculate. The value table needs two columns, where the first one is the name of the field to calculate statistics on, and the second contains the statistic to calculate. The documentation specifies that like this: \n",
    "\n",
    "```\n",
    "[[field, {statistic_type}],...]\n",
    "```\n",
    "\n",
    "Create a value table that specifies the following statistics, all on a field called POPLASTCEN:\n",
    "\n",
    "- minimum\n",
    "- maximum\n",
    "- mean\n",
    "- standard deviation\n",
    "\n",
    "**Don't run the Statistics tool, but store your value table in a variable, because you're going to use it in problem 2.**\n",
    "\n",
    "**You will need to follow the link to the documentation for details on how to specify the statistics to use. And you don't need an actual dataset in order to create the value table.** Your value table will have two columns and four rows (or four smaller lists inside one big list). You can use a list, string, or value table object-- whichever you prefer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Use your value table from problem 1 to calculate [Summary Statistics](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/summary-statistics.htm) for cities.shp. Call your output table 'city_stats.dbf'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This next bit of code will print out the contents of your city_stats.dbf file. It should look like this:\n",
    "\n",
    ".|OID|FREQUENCY|MIN_POPLAS|MAX_POPLAS|MEAN_POPLA|STD_POPLAS\n",
    "---|---|---|---|---|---|---\n",
    "0|0|248|-1.0|181743.0|7546.334677|19487.692503"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "classtools.table2pd('city_stats.dbf').head()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "267.641px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
