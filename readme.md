# Value tables and selection sets

The first thing you'll see this week are *value tables*, which help you set up input parameters for geoprocessing tools that can take multiple items for a single parameter (such as the Union tool, which takes a variable-length list of feature classes to union together).

The second thing you'll learn is how to select features using code, and how to take advantage of that ability.

See the [homework description](markdown/homework.md).
