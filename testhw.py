import os
from tests.testtools import BaseTester, Runner

__unittest = True

def test_results(problem):
    if problem == 1:
        Runner.test_output(1, Problem1R)
    elif problem == 2:
        Runner.print('This problem has no output to test')
    elif problem == 3:
        Runner.test_output(3, Problem3R)

def test_notebook(problem):
    if problem == 1:
        Runner.run_problem(1, [Problem1])
    elif problem == 2:
        if Runner.run_problem(2, [Problem2A, Problem2B]):
            Runner.print('Check tests/problem2.ipynb to make sure it found 40 deer instead of 16.')
    elif problem == 3:
        Runner.run_problem(3, [Problem3A, Problem3B])


class Problem1(BaseTester):
    problem = 1
    args = {}
    result_filename = 'stats_test.dbf'
    test_filename = 'stats.csv'

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            city_filename='cities.shp',
            county_filename='county.shp',
            stats_filename=self.result_filename,
        )

    def test_1_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        self.result_exists()
        result_df = self._import_result()
        self.assertEqual(len(result_df), 29, f'Output has {len(result_df)} rows instead of 29')

    def test_2_columns(self):
        """Test output columns"""
        self.notebook_ran()
        self.result_exists()
        test_df = self._import_test_data()
        result_df = self._import_result()
        self.assertEqual(sorted(result_df.columns), sorted(test_df.columns),
            f'Output columns are {list(result_df.columns)} instead of {list(test_df.columns)}')

    def test_3_data(self):
        """Test output values"""
        self.notebook_ran()
        self.result_exists()
        test_df = self._import_test_data()
        result_df = self._import_result().filter(test_df.columns)
        self.assertTrue(result_df.equals(test_df), 'Output data values look wrong')

    def _import_result(self):
        import pandas as pd
        import arcpy
        with arcpy.da.SearchCursor(self.result_path, '*') as rows:
            df = (
                pd.DataFrame(data=list(rows), columns=rows.fields)
                .drop('OID', axis='columns')
                .rename(columns=lambda x: x.split('_')[0])
                .sort_values('county')
            )
        cols = ['MIN', 'MAX', 'MEAN', 'STD']
        df[cols] = df[cols].round(2)
        return df

    def _import_test_data(self):
        import pandas as pd
        return pd.read_csv(os.path.join(self.test_folder, 'data', self.test_filename))


class Problem1R(Problem1):
    result_filename = 'stats.dbf'

    def notebook_ran(self):
        return True


class Problem2(BaseTester):
    problem = 2
    args = {}
    filename = 'deer_test.shp'

    def setUp(self):
        super().setUp()
        self.args = dict(
            filename=os.path.join(self.data_folder, self.filename),
            point_id=self.point_id,
            distance=self.distance,
        )


class Problem2A(Problem2):
    suffix = 'a'
    point_id = 375
    distance = 200


class Problem2B(Problem2):
    suffix = 'b'
    point_id = 400
    distance = 500


class Problem3(BaseTester):
    problem = 3
    args = {}
    result_filename = 'web_cities.shp'
    county = ''
    rows = 0

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder = self.data_folder,
            city_filename='cities_test.shp',
            county_filename='county_test.shp',
            output_filename=self.result_filename,
            county_name=self.county,
        )

    def test_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        n = int(arcpy.GetCount_management(self.result_path).getOutput(0))
        self.assertEqual(n, self.rows, f'Output has {n} rows instead of {self.rows}')

    def test_srs(self):
        """Test spatial reference"""
        self.notebook_ran()
        self.result_exists()
        import arcpy
        srs = arcpy.Describe(self.result_path).spatialReference.name
        self.assertEqual(srs, 'GCS_WGS_1984', f'Output SRS is {srs} but should be GCS_WGS_1984')
        x, y = next(arcpy.da.SearchCursor(self.result_path, 'SHAPE@XY'))[0]
        self.assertTrue(-180 < x and x < 0 and 0 < y and y < 90, 'Output coordinates are not lat/lon')


class Problem3A(Problem3):
    suffix = 'a'
    county = 'SALT LAKE'
    rows = 11


class Problem3B(Problem3):
    suffix = 'b'
    county = 'UTAH'
    rows = 22


class Problem3R(Problem3A):
    result_filename = 'salt_lake_cities.shp'


if __name__ == '__main__':
    test_notebook(1)
    test_notebook(2)
    test_notebook(3)
