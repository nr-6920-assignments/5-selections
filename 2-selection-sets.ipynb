{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 5-selections\\data folder.\n",
    "data_folder = r'D:\\classes\\NR6920\\Assignments\\5-selections\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import arcpy and set the workspace.\n",
    "import arcpy\n",
    "arcpy.env.workspace = data_folder\n",
    "arcpy.env.overwriteOutput = True\n",
    "\n",
    "# Tell ArcGIS to look in your assignment folder for modules so that\n",
    "# it can find classtools. Jupyter will already look there and \n",
    "# doesn't actually need this.\n",
    "import os\n",
    "import sys\n",
    "sys.path.append(os.path.dirname(arcpy.env.workspace))\n",
    "\n",
    "# Import classtools.\n",
    "import classtools\n",
    "\n",
    "# Set up plotting in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What are selection sets?\n",
    "\n",
    "You probably know that if you run a geoprocessing tool in ArcGIS while you have features selected, the tool will only use the selected features. What if you need to replicate this behavior in a script, when ArcGIS isn't even open?\n",
    "\n",
    "You have a few options.\n",
    "1. If the tool you want to use has a `where_clause` parameter, then you can use that to select which features to use.\n",
    "2. You could turn your script into a tool, but you haven't learned how to do that yet.\n",
    "3. Create your own **selection set**, which is just like selecting features, but from Python code. That's what you'll learn about here.\n",
    "\n",
    "**You're in luck!** It turns out that selections sets are a lot easier than they used to be. I didn't know this until last semester when a few students did it \"wrong\" in their homework, but it still worked. I'm not sure when it changed, but I tested with ArcMap and that still requires the old way. I'll put info about that at the end of the notebook so that you have in case you need to write code for ArcMap (or old versions of Pro, assuming they also require the hard way, but I honestly don't know)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Select by attribute\n",
    "\n",
    "Let's start with creating a selection set by using an attribute. How about getting the cities with a population over 50,000? If you've forgotten what fields are in the cities shapefile, you can use [ListFields](https://pro.arcgis.com/en/pro-app/arcpy/functions/listfields.htm) to find out. Let's do that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loop through the list of fields in city_layer (cities.shp)\n",
    "# and print out each one's name.\n",
    "for field in arcpy.ListFields('cities.shp'):\n",
    "    print(field.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The POPLASTCEN (population last census) field should work to get cities with a certain population. But in order to make your selection, you'll need a query that tells the tool which features you'd like to select. This is a string that's a lot like a SQL `WHERE` clause, if you're familiar with that. If not, here's the [SQL reference for query expressions used in ArcGIS](https://pro.arcgis.com/en/pro-app/help/mapping/navigation/sql-reference-for-elements-used-in-query-expressions.htm).\n",
    "\n",
    "Here's the syntax for [SelectLayerByAttribute](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/select-layer-by-attribute.htm):\n",
    "\n",
    "```\n",
    "arcpy.management.SelectLayerByAttribute(in_layer_or_view, {selection_type}, \n",
    "    {where_clause}, {invert_where_clause})\n",
    "```\n",
    "\n",
    "You're interested in these parameters:\n",
    "\n",
    "- `in_layer_or_view`: The dataset that you want to make the selection on, which is `cities.shp`.\n",
    "- `selection_type`: The type of selection you want to make. You don't want to build on a preexisting selection, so you can ignore this parameter and use the default value of `NEW_SELECTION`.\n",
    "- `where_clause`: The selection criteria. You'll use `POPLASTCEN > 50000` in order to select cities with a population above 50,000."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select the cities where POPLASTCEN > 50000.\n",
    "large_cities = arcpy.management.SelectLayerByAttribute(\n",
    "    in_layer_or_view='cities.shp', \n",
    "    where_clause='POPLASTCEN > 50000'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the `large_cities` variable is an arcpy [Result](https://pro.arcgis.com/en/pro-app/latest/arcpy/classes/result.htm) object that contains two things:\n",
    "\n",
    "1. A *feature layer* with cities over 50,000 people selected. The `cities.shp` shapefile isn't altered in any way, and you *must* use this new feature layer if you want to run a tool on the selected cities only.\n",
    "2. The number of selected features.\n",
    "\n",
    "With newer versions of ArcGIS, you view the contents of this Result object like this (if this doesn't show you a table containing \"a Layer object\" and \"9\", then you must have an older version):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "large_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see just the number of features selected, you can use the Result's `getOutput()` method or the [GetCount()](https://pro.arcgis.com/en/pro-app/latest/tool-reference/data-management/get-count.htm) geoprocessing tool. This shows how to use both of those methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Using getOutput():', large_cities.getOutput(1))\n",
    "print('Using GetCount():', arcpy.management.GetCount(large_cities))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even though the Result object already knows how many features it has, I kind of like using `GetCount()` instead because it's more obvious what's happening.\n",
    "\n",
    "You can also use `GetCount()` to see how many features are in the original shapefile (you can't use `getOutput()` for this one because 'cities.shp' isn't a Result object):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(arcpy.management.GetCount('cities.shp'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even though `large_cities` is a Result object, you can use it with any geoprocessing tools and ArcPy is smart enough to use the feature layer contained inside the Result. So now you can use [Feature Class To Feature Class](https://pro.arcgis.com/en/pro-app/tool-reference/conversion/feature-class-to-feature-class.htm) to save the selected cities to a new file called large_cities.shp. You can use the current workspace (the data folder) for the output location (the `out_path` parameter)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the the selected features in large_cities to large_cities.shp.\n",
    "print(arcpy.conversion.FeatureClassToFeatureClass(\n",
    "    in_features=large_cities, \n",
    "    out_path=arcpy.env.workspace, \n",
    "    out_name='large_cities.shp',\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use classtools to plot the new shapefile in yellow, on top of the original in blue. Remember that classtools doesn't actually have anything to do with ArcPy-- it's just something I wrote to help you visualize what's going on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the new shapefile (yellow) on top of the original (blue).\n",
    "classtools.plot(\n",
    "    data=['cities.shp', 'large_cities.shp'], \n",
    "    symbols=['blue', 'yellow'], \n",
    "    extent='large_cities.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new shapefile should only contain the 9 selected features from the original. Let's make sure that's the case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Count the number of features in the new shapefile.\n",
    "print(arcpy.management.GetCount('large_cities.shp'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Select by location\n",
    "\n",
    "If you want to select by spatial location instead of by attribute, you need two feature layers: one to be selected on, and one to define the area to select. \n",
    "\n",
    "Let's use `cache.shp` to select the cities in Cache County. Here's the syntax for the [Select Layer By Location](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/select-layer-by-location.htm) tool:\n",
    "\n",
    "```\n",
    "arcpy.management.SelectLayerByLocation(in_layer, {overlap_type},\n",
    "    {select_features}, {search_distance}, {selection_type}, \n",
    "    {invert_spatial_relationship})\n",
    "```\n",
    "\n",
    "You're interested in these parameters:\n",
    "\n",
    "- `in_layer`: The layer to select features in. You'll use `cities.shp`.\n",
    "- `overlap_type`: The type of spatial relationship to use for the selection. The default is `INTERSECT`, but you want to get the cities that fall within Cache County, so you'll use `WITHIN` instead of the default.\n",
    "- `select_features`: The layer that determines what spatial locations to select. You'll use `cache.shp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cache_cities = arcpy.management.SelectLayerByLocation(\n",
    "    in_layer='cities.shp', \n",
    "    overlap_type='WITHIN',\n",
    "    select_features='cache.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How many cities are selected?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(arcpy.management.GetCount(cache_cities))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's save those selected cities to a new shapefile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the the selected features in cache_cities to cache_cities.shp.\n",
    "print(arcpy.conversion.FeatureClassToFeatureClass(\n",
    "    in_features=cache_cities, \n",
    "    out_path=arcpy.env.workspace, \n",
    "    out_name='cache_cities.shp',\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's use classtools to draw the new shapefile along with the original datasets so we can make sure they look correct. We'll add the county boundary to the plot, too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the new shapefile (yellow) on top of the original (blue).\n",
    "classtools.plot(\n",
    "    data=['cache.shp', 'cities.shp', 'cache_cities.shp'], \n",
    "    symbols=['white', 'blue', 'yellow'], \n",
    "    extent='cache.shp',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What's the deal with Cornish up at the top? It's blue, but it sure looks like it should be yellow because it falls within Cache County. This is a good example of why you should always confirm your results as much as possible. The city and county outlines must not quite line up and there must be a little bit of Cornish that isn't in Cache County, at least according to these two datasets (but probably not in reality, because I doubt the town boundaries cross into Idaho!). But because of that, ArcGIS must not consider it to be `WITHIN` Cache County."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if you didn't have a shapefile that contained just Cache County and instead needed to use the shapefile with all counties? You could use `SelectLayerByAttribute()` to get the correct county by name, and then use that instead of county.shp with `SelectLayerByLocation()`. Let's try that with Washington County. The first step is to get a feature layer that has Washington County selected. This is similar to the earlier `SelectLayerByAttribute()` example, except that SQL statements require that you put single quotes around the string you're trying to match. So the `where_clause` needs to look like this:\n",
    "\n",
    "```\n",
    "NAME = 'WASHINGTON'\n",
    "```\n",
    "\n",
    "The county names in the shapefile's attribute table are in all caps, so matching against 'Washington' wouldn't find anything. Also notice that this uses a single equal sign. Although most programming languages use a double equal sign to check for equality, SQL uses a single one, and the `where_clause` is SQL instead of Python. Confused? Relational databases use SQL for their query language. Python is just passing this SQL to the underlying database so it can peform the query and then give the results back to Python. Since the database is what's doing the work here, the query statement needs to be in its language."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select Washington County by name.\n",
    "washington = arcpy.management.SelectLayerByAttribute(\n",
    "    in_layer_or_view='county.shp', \n",
    "    where_clause=\"NAME = 'WASHINGTON'\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the fun of it, let's use classtools to look at the attribute table for the selected feature. You can see that there is only one feature, and it corresponds to WASHINGTON County."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "classtools.table2pd(washington)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use `washington` to select the cities in Washington County and then print out how many cities were actually selected. We'll use 'INTERSECT' this time in order to get all cities that intersect with Washington County."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "washington_cities = arcpy.management.SelectLayerByLocation(\n",
    "    in_layer='cities.shp', \n",
    "    overlap_type='INTERSECT',\n",
    "    select_features=washington,\n",
    ")\n",
    "print(f'{arcpy.management.GetCount(washington_cities)} cities selected')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And take a look graphically, just to prove it worked. This time you're just plotting the selected features instead of saving them to a shapefile first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the extracted features(yellow) on top of the original (blue).\n",
    "classtools.plot(\n",
    "    data=[washington, 'cities.shp', washington_cities], \n",
    "    symbols=['white', 'blue', 'yellow'], \n",
    "    extent=washington,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Select by both attribute and location\n",
    "\n",
    "To select by both attribute and location, simply use your existing selection set with a second selection method and change the selection type to `SUBSET_SELECTION` for the second selection.\n",
    "\n",
    "As an example, let's find the cities in Rich County that have a population less than 300. I'll use variables so that you can see how easy it is to change the selection criteria if you set things up like that (which is I why I insist you use variables in your homework!). Read the code comments to see what's going on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "county_name = 'RICH'\n",
    "max_population = 300\n",
    "\n",
    "# Use an attribute selection to select the county of interest from the county shapefile.\n",
    "county = arcpy.management.SelectLayerByAttribute(\n",
    "    in_layer_or_view='county.shp', \n",
    "    where_clause=f\"NAME = '{county_name}'\",\n",
    ")\n",
    "print('Counties selected:', arcpy.management.GetCount(county))\n",
    "\n",
    "# Use a location selection to select the cities that intersect the selected county.\n",
    "cities = arcpy.management.SelectLayerByLocation(\n",
    "    in_layer='cities.shp', \n",
    "    select_features=county,\n",
    ")\n",
    "print(f'{arcpy.management.GetCount(cities)} cities selected in {county_name} County')\n",
    "\n",
    "# Now use an attribute selection to select the cities with a small population\n",
    "# FROM THE ALREADY SELECTED CITIES instead of cities.shp.\n",
    "cities = arcpy.management.SelectLayerByAttribute(\n",
    "    in_layer_or_view=cities, \n",
    "    selection_type='SUBSET_SELECTION',\n",
    "    where_clause=f'POPLASTCEN < {max_population}',\n",
    ")\n",
    "print(f'{arcpy.management.GetCount(cities)} cities selected in {county_name} County with a population less than {max_population}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see how easy it is to change the final selection, try changing the `max_population` or `county_name` variable and rerun the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "# Problem 1\n",
    "\n",
    "This next code selects the cities in Cache County. You need to **add to** the `cities` selection so that all county seats in the state are selected in addition to the cities already selected (you want the ones where the COUNTYSEAT attribute is 1). You'll need to look at the [documentation](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/select-layer-by-attribute.htm) to figure out how add to the current selection. You just need to add one line of code to the following cell. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "# This selects the cities in Cache County.\n",
    "cities = arcpy.management.SelectLayerByLocation(in_layer='cities.shp', overlap_type='WITHIN', select_features='cache.shp')\n",
    "print(f'{arcpy.management.GetCount(cities)} cities selected in Cache County')\n",
    "\n",
    "# Add your line of code here.\n",
    "\n",
    "\n",
    "# Print out the number of selected cities.\n",
    "print(f'{arcpy.management.GetCount(cities)} cities selected after adding county seats')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "# Problem 2\n",
    "\n",
    "That started off with 18 cities in Cache County selected, but now you should gave 46 cities selected from all around the state. Why aren't they limited to Cache County anymore? (This is not a trick question. It's pretty simple and has to do with the line of code you added.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "*Double-click here to add your answer.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ArcMap\n",
    "\n",
    "With ArcMap (and possibly old versions of ArcGIS Pro), you can't use filenames with `SelectLayerByAttribute()` and `SelectLayerByLocation()`. Instead, you need to manually create a feature layer to select from first. You can do that with the [Make Feature Layer tool](https://pro.arcgis.com/en/pro-app/latest/tool-reference/data-management/make-feature-layer.htm).\n",
    "\n",
    "```\n",
    "arcpy.management.MakeFeatureLayer(in_features, out_layer, {where_clause}, \n",
    "    {workspace}, {field_info})\n",
    "```\n",
    "\n",
    "There are two required parameters to use this. The `in_features` parameter is the filename of the dataset you want to use (like cities.shp), while `out_layer` is a name for your new feature layer. This is like a filename, but in memory. You can use it to access the feature layer later if you want. Here's an example that creates a feature layer from county.shp. It puts it into a variable called `county_layer`, but it also creates a sort of filename in memory called 'counties'.\n",
    "\n",
    "*And even though the new syntax style works in ArcMap, I'm going to use the old underscore style so you can get the full effect!*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a feature layer.\n",
    "county_layer = arcpy.MakeFeatureLayer_management(in_features='county.shp', out_layer='counties')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use either the `county_layer` variable or the 'counties' filename to access your feature layer. Let's count how many features it has using both methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('county_layer:', arcpy.GetCount_management(county_layer))\n",
    "print(\"'counties':\", arcpy.GetCount_management('counties'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, you don't even need to create the `county_layer` variable if you want to use 'counties' to access your feature layer in the rest of your code. You could just do this instead:\n",
    "\n",
    "```python\n",
    "arcpy.management.MakeFeatureLayer(in_features='county.shp', out_layer='counties')\n",
    "```\n",
    "\n",
    "Anyway, now that you have your feature layer, you can make a selection on it. Not only did you have to create feature layers in the past, but you had to surround the column name in your `where_clause` with double quotes if selecting from a shapefile. So instead of looking like `NAME = 'DAVIS'`, it would need to look like `\"NAME\" = 'DAVIS'`. Because of that, the `where_clause` in the example uses escape characters to insert these double quotes (see the strings notebook from the second week if you need a refresher).\n",
    "\n",
    "```python\n",
    "where_clause=\"\\\"NAME\\\" = 'DAVIS'\"\n",
    "```\n",
    "\n",
    "*I just checked with ArcMap and it worked without the double quotes, but I promise that it didn't use to! I'm going to include them for the same reason I'm using the underscores.*\n",
    "\n",
    "Anyway...you don't need to save the selection result into a variable because it works on the feature layer you already made. This next example selects Davis County and then counts the selected features, just to prove that now there's only one instead of 29."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcpy.SelectLayerByAttribute_management(\n",
    "    in_layer_or_view=county_layer, \n",
    "    where_clause=\"\\\"NAME\\\" = 'DAVIS'\",\n",
    ")\n",
    "print(arcpy.GetCount_management(county_layer))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the example from above that selected by attribute and location, but doing it by creating feature layers first.\n",
    "\n",
    "*f-strings don't exist in Python 2.7, which is what ArcMap uses, so I'm going to change those to use `format()` instead.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "county_name = 'RICH'\n",
    "max_population = 300\n",
    "\n",
    "# Create a feature layer for the counties.\n",
    "counties = arcpy.MakeFeatureLayer_management(in_features='county.shp', out_layer='counties')\n",
    "\n",
    "# Create a feature layer for the cities.\n",
    "cities = arcpy.MakeFeatureLayer_management(in_features='cities.shp', out_layer='cities')\n",
    "\n",
    "# Use an attribute selection to select the county of interest from the counties feature layer.\n",
    "arcpy.SelectLayerByAttribute_management(\n",
    "    in_layer_or_view=counties, \n",
    "    where_clause=\"\\\"NAME\\\" = '{0}'\".format(county_name),\n",
    ")\n",
    "print('Counties selected:', arcpy.GetCount_management(counties))\n",
    "\n",
    "# Use a location selection to select the cities that intersect the selected county.\n",
    "arcpy.SelectLayerByLocation_management(\n",
    "    in_layer=cities, \n",
    "    select_features=counties,\n",
    ")\n",
    "print('{0} cities selected in {1} County'.format(arcpy.GetCount_management(cities), county_name))\n",
    "\n",
    "# Now use an attribute selection to select the cities with a small population\n",
    "# FROM THE ALREADY SELECTED CITIES.\n",
    "arcpy.SelectLayerByAttribute_management(\n",
    "    in_layer_or_view=cities, \n",
    "    selection_type='SUBSET_SELECTION',\n",
    "    where_clause='\"POPLASTCEN\" < {0}'.format(max_population),\n",
    ")\n",
    "print('{0} cities selected in {1} County with a population less than {2}'.format(\n",
    "    arcpy.GetCount_management(cities), county_name, max_population))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's another version that creates the `counties` feature layer with the correct county already selected and the `cities` feature layer with the small cities already selected, which gets rid of several steps and makes it the same length as the one that works with new versions of ArcGIS Pro."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a feature layer for the counties that already has the correct county selected.\n",
    "counties = arcpy.MakeFeatureLayer_management(\n",
    "    in_features='county.shp', \n",
    "    out_layer='counties', \n",
    "    where_clause=\"\\\"NAME\\\" = '{0}'\".format(county_name),\n",
    ")\n",
    "print('{0} county originally selected by name'.format(arcpy.GetCount_management(counties)))\n",
    "\n",
    "# Create a feature layer for the cities that already as the small cities selected.\n",
    "cities = arcpy.MakeFeatureLayer_management(\n",
    "    in_features='cities.shp', \n",
    "    out_layer='cities',\n",
    "    where_clause='\"POPLASTCEN\" < {0}'.format(max_population),\n",
    ")\n",
    "print('{0} cities originally selected with a population less than {1}'.format(\n",
    "    arcpy.GetCount_management(cities), max_population))\n",
    "\n",
    "# Use a location selection to select the cities that intersect the selected county.\n",
    "# Make sure you subset the current small city selection so you don't get all cities in\n",
    "# the county instead.\n",
    "arcpy.SelectLayerByLocation_management(\n",
    "    in_layer=cities, \n",
    "    select_features=counties,\n",
    "    selection_type='SUBSET_SELECTION'\n",
    ")\n",
    "print('{0} cities selected in {1} County with a population less than {2}'.format(\n",
    "    arcpy.GetCount_management(cities), county_name, max_population))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
