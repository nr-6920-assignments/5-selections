# Selection set & value table homework

*Due Monday, February 22, at midnight*

You can use Jupyter or ArcGIS to read this week's notebooks. I've already loaded the data and notebooks into the `selections.aprx` project.

## Notebook comprehension questions

*Worth 6 points*

Work through the notebooks in this order:

- 1-value-tables.ipynb (3 points)
- 2-selection-sets.ipynb (3 points)

## Script problems

*Worth 10 points each*

**There is a `run-tests.ipynb` notebook again this week. Please use it to check your results!**

One of the great things about notebooks is that you can store results right with your code, and that also makes grading a bit easier. **If you want full credit, make sure you include some explanation of what you're doing and all of the tables and plots I ask for.** And of course you can include others that you find useful, as long as your notebook isn't filled up with a bunch of code that you were just using to test things out with. There should be a reason for everything included in your notebook.


## Problem 1

Create a notebook called `problem1`. Set up four variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\5-selections\data'
city_filename = 'cities.shp'
county_filename = 'county.shp'
stats_filename = 'stats.dbf'
```

Nothing else should be in that first cell, and these filenames should not appear anywhere else in your notebook. Always use the variables that you set here.

Your notebook needs to create a dbf file (`stats_filename`) that contains statistics for the POPLASTCEN field in the city file, but grouped by county name.

Include these statistics for each county:

- minimum
- maximum
- mean
- standard deviation

You're in luck. With ArcMap, this problem required creating feature layers (like in the ArcMap section of the selection-sets notebook) to use with the Add Join tool, but that's not required anymore and now you can use filenames. Nice! The Add Join tool will return a Result object that contains a feature layer, just like the Select Layer by Attribute and Location tools in the selection-sets notebook. You can use this with other geoprocessing tools, just like you did with the layers in the selection-sets notebook. The problem requires two main steps. 

1. Join the county shapefile to the city shapefile using [Add Join](https://pro.arcgis.com/en/pro-app/tool-reference/data-management/add-join.htm). The `COUNTYNBR` field is common to both shapefiles.
    1. After joining, use `classtools.table2pd(your_feature_layer).head()` in order to print out the first few rows of the table (but change `your_feature_layer` to the appropriate value, of course!). If you don't see fields from both the cities and county shapefiles, then your join didn't work. **I want to see this output in your notebook.**
2. Use [Summary Statistics](https://pro.arcgis.com/en/pro-app/tool-reference/analysis/summary-statistics.htm) on the joined feature layer in order to create the requested statistics file. You'll need to create a value table much like the one you created for problem 1 in the value-tables notebook. 
   1. Use `classtools.table2pd(stats_filename)` to print out your new dbf file. If it doesn't look like the screenshot below then something is wrong. Make sure the numbers match!

### Hints

1. After the join from step 1, field names in the joined feature layer will all be prefixed with the shapefile name that they originally came from. For example, the POPLASTCEN field will be called 'cities.POPLASTCEN' and the county name field will be called 'county.NAME'. This is how you need to refer to the fields when calculating statistics.
2. Read the documentation for the Summary Statistics tool carefully in order to figure out how to group by county.

*This notebook will make a lot of assumptions about the files it's working with. In the real world, that may or may not be a good idea, depending on your use case. If you have a whole bunch of files with the same structure and you need to do the exact same thing with each of them, then something like this would make sense. Code like this would be a bad idea if the files didn't all have the same field names or you wanted to calculate statistics on different fields in different files, however-- in that case you'd want to add more variables for field name and statistics.*

### Results

Don't forget to check your results with the `run-tests` notebook after you've checked them visually against this screenshot.

![problem 1 results](images/p1_results.png)

## Problem 2

Create a notebook called `problem2`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
filename = r'D:\classes\NR6920\Assignments\5-selections\data\deer.shp'
point_id = 375
distance = 200
```

Nothing else should be in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

This notebook needs to use selection sets to find out how many deer locations in the shapefile are within a given distance of a specific location point (the one with the provided ID). Assume that there's a field in the shapefile called 'id'. For example, you'll test it by finding the number of features in deer.shp that are within 200 meters of the point with id=375. Print out the number of points that meet this condition.

You can assume that the distance provided is in the same map units as the shapefile, so you don't need to worry about units when telling arcpy what distance to use. (The deer shapefile is UTM, so the map units are meters.) 

Here's a basic outline:

1. Because you have a full path and filename in the `filename` variable, you don't need to worry about a folder or arcpy workspace.
2. Use `SelectLayerByAttribute()` to select the correct point in the shapefile. Make sure you create your `where_clause` using the `point_id` variable.
3. Use `GetCount()` to make sure there is one feature selected.
4. Use `SelectLayerByLocation()` to select features in the shapefile that are within `distance` of the point selected in  step 2.
5. Use `GetCount()` to make sure there are **16 features** selected.
6. Plot the selections with this code (**but use your feature layer names**). The 16 selected points will be cyan, the point with id 375 will be red, and the rest will be a yellowish color:

```python
classtools.plot([filename, step_4_feature_layer, step_2_feature_layer], ['yo', 'co', 'ro'])
```
Your plot should look like this:

![problem 2](images/p2_results.png)

Don't forget to use `run-tests` to check that your notebook runs successfully with other variables.

## Problem 3

Web maps often want the user to provide vector data using geographic WGS84 coordinate systems, and then the app converts it to the Web Mercator projection that's used for most web maps. This notebook is going to extract the cities that fall entirely within a county and reproject them to WGS84 so that it can be used with a web map.

Create a notebook called `problem3`. Set up five variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\5-selections\data'
city_filename = 'cities.shp'
county_filename = 'county.shp'
output_filename = 'salt_lake_cities.shp'
county_name = 'SALT LAKE'
```

Nothing else should be in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

The notebook should select the cities from `city_filename` that are **completely** within the county specified with `county_filename` and `county_name`, and [project](https://pro.arcgis.com/en/pro-app/latest/tool-reference/data-management/project.htm) them to a new shapefile that uses WGS84 geographic coordinates (`output_filename`). **DO NOT create any intermediate files-- use the selection set to determine which features are reprojected.**

1. The field in county.shp that contains names is called 'NAME'. 
2. After selecting the cities in the appropriate county, but before reprojecting them, use `classtools.plot(your_feature_layer)` to plot the selected cities.
3. The EPSG code (WKID) for WGS84 is 4326.
4. Use the `WGS_1984_(ITRF00)_To_NAD_1983` datum transformation when reprojecting. **Students in the past have had a habit of missing this instruction. Don't be like them!**
5. After reprojecting, use `Describe()` (from the intro notebook from last week) to find and print the spatial reference name for the new shapefile. If it doesn't match `GCS_WGS_1984` then you've got a problem.
6. Use `GetCount()` to see how many features are in your new shapefile (use the `output_filename` variable so that you're looking at the actual shapefile and not your feature layer). **There should be 11.**
7. Use `classtools.plot(output_filename)` to plot your output file. Compare this to your plot from step 2. They should look different and have different coordinates along the axes.
8. In the past I had people add a web map in addition to the classtools plots, since that's theoretically why you extracted the data in the first place. However, we used a different Python setup then, and ArcGIS doesn't install the module that I used for the web maps. So we're going to cheat and use something that's part of the Python ArcGIS API (different than ArcPy) and make a webmap with that. The HUGE difference is that with this one, the data don't have to use a particular spatial reference, so the whole reprojection wasn't necessary. So this is cheating (and most web maps really do make you use a specific spatial reference). Use this code and be patient, because it will probably take it a while. After it draws, try clicking on a city and it'll show the attributes for that feature.

```python
import arcgis
import pandas

pandas.DataFrame.spatial.from_featureclass(output_filename).spatial.plot()
```

Don't forget to use `run-tests` to check your results and that your notebook runs successfully with other variables.
